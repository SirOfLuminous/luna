const s = require('dgram').createSocket('udp4')
const b = Buffer.alloc(1)

var recv_count = 0
var sent_count = 0
var last_sent = 0
var last_loss =0 
s.bind(1234)
s.on("message",(msg,rinfo)=>{
    recv_count++
})
function doSend(){
    for (let i = 0; i < 8; i++){
        sent_count++
        s.send(b, 1234)
    }
    //simluate fill buffer in real usage
    //Also let the recvhandler and timer have chance to print out
    setImmediate(()=>{doSend()})   
}

function doSendInCallback(){
    sent_count++
    s.send(b, 1234,()=>{doSendInCallback()})
}
setInterval(()=>{
    console.log("GAP>%d,GAP Increased %d, total s:%d r:%d,send rate %d package/sec,loss rate% %d ",sent_count-recv_count,
                                                               sent_count-recv_count-last_loss,
                                                                sent_count,
                                                                recv_count,
                                                                sent_count-last_sent,
                                                                ((sent_count-recv_count-last_loss)*100/(sent_count-last_sent)).toFixed(3))
    last_sent = sent_count
    last_loss = sent_count-recv_count
},1000)
doSend()
//to compare with 
//doSendInCallback()