
# Luna

    
  A small abstraction layer to serialize packets using NodeJS __UDP__ core to make fast volatile messaging even simpler.

  Luna uses the tiny [AMP](https://github.com/visionmedia/node-amp) prototol to serialize buffer, string,
  and json arguments.

## Installation

```
$ npm install -S git+ssh://git@bitbucket.org:SirOfLuminous/luna.git
```

## Example

  A small in-proc example of a server with three clients:

```js
var luna = require('luna');
var server = luna.bind('0.0.0.0:5000');
var a = luna.connect('0.0.0.0:5000');
var b = luna.connect('0.0.0.0:5000');
var c = luna.connect('0.0.0.0:5000');

server.on('message', function(msg){
  console.log(msg);
});

setInterval(function(){
  a.send({ hello: 'world' });
}, 150);

setInterval(function(){
  b.send('hello world');
}, 150);

setInterval(function(){
  c.send(Buffer.from('hello'));
}, 150);
```

  yielding:

```
<Buffer 68 65 6c 6c 6f>
hello world
{ hello: 'world' }
<Buffer 68 65 6c 6c 6f>
hello world
{ hello: 'world' }
...
```
## API

### Server(addr)

  Bind to the given `addr`.

### Client(addr)

  Connect to the given `addr`.

### Client#send(...)

  Send one or more arguments a single atomic message. The following
  types are supported through AMP:

  - strings
  - buffers
  - objects (serialized as JSON)

## License

  MIT